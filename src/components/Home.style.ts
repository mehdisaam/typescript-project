import styled from 'styled-components'

export const MainHome = styled.div`
height:100vh;
width:100%;
display:flex;
background:black;
position:absolute;
margin:0;
padding:0;
box-sizing:border-box;
top:0;
right:0;
left:0;
bottom:0;
`

export const WraperPrim = styled.div`

background:#D9339C;
position:absolute;
width: 167.65px;
    height: 107.43px;
    left: 5.48px;
    top: 137.92px;
    background: #D9339C;
    opacity: 1;
    filter: blur(66px);
transform: rotate(55deg);
`
export const WraperBlue = styled.div`
background:#D9339C;
position:absolute;
width: 203.95px;
    height: 206.8px;
    left: 77px;
    top: 73.4px;
background: #37DBF3;
opacity: 0.3;
filter: blur(141px);
transform: rotate(75deg);

`
export const WraperBottomBlue = styled.div`
position: absolute;
width: 207.65px;
height: 179.97px;
left: -54.86px;
top: 150.15px;
background: #37DBF3;
opacity: 0.7;
filter: blur(105px);
transform: rotate(-45deg);
`
export const ContentMain = styled.div`
width:92%;
padding:10px;
position:absolute;
background-color:transpanet;
top:36%;
display:flex;
justify-content:center;
align-items:center;
color:#fff;
@media (min-width: 768px){
    justify-content:flex-start;
}
`
