import styled from 'styled-components'


export const ContentMenu = styled.div`
display:flex;
flex-wrap:wrap;
flex-direction:column;
align-items:center;
width:100%;
height:100%;
@media (min-width: 768px){
    align-items: flex-end;
    padding-right: 11px;
}
ul{
    list-style-type:none;
    width:82%;
    height:100%;
    text-align:center;
    li{
        width: 100%;
        padding: 16px 0;
        &:nth-of-type(5){
            font-weight: bold;
            font-size: 14px;
            line-height: 20px;
            color: #37DBF3;
            width:100%;
            padding:0;
            height:44px;
            background-color:transparent;
            border:none;
            border:1px dotted #37DBF3;
            display: flex;
            border-radius:5px;
    justify-content: center;
    align-items: center;
    span{
        margin-right: 8px;
    }
    @media (min-width: 1380px){
        width:40%;
    }
    @media (min-width: 780px){
        width:45%;
    }
        }
        &:nth-of-type(6){
            font-weight: bold;
            font-size: 14px;
            line-height: 20px;
            color: #fff;
            width:100%;
            background-color:#B53385;
            border-radius:5px;
            padding:0;
            margin-top:15px;
            height:41px;
            border:none;
            border:1px dotted #B53385;
            display: flex;
    justify-content: center;
    align-items: center;
    span{
        margin-right: 8px;
    }
    @media (min-width: 1380px){
        width:40%;
    }
    @media (min-width: 768px){
        margin-top:0;
        margin-left:10px;
        width:45%;
    }
        }
        @media (min-width: 768px) {
            width:40%;
        }
    }
    @media (min-width: 768px) {
    display: flex;
    flex-direction: row;
    margin-top:0
    }
}
`
export const HeaderTop = styled.div`
    display: flex;
    flex-direction: row-reverse;
    justify-content: space-between;
    align-items: center;
    height:51px;
    div{
        margin-left:10px;
    }
`



