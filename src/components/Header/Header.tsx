import React, { FC, useState } from 'react'
import { MenuData } from './DataMenu'
import { ContentMenu,HeaderTop } from './Header.style'
import LogoIcon from './LogoIcon';
import styled from 'styled-components'
import {isMobile} from 'react-device-detect';
const Header = () => {
  const [isOpen, setIsOpen] = useState<boolean>(false)
  const HeaderMain = styled.div`
  width: 95%;
      background: black;
      height:${isOpen ? "57%" : '51px'};
      position: absolute;
      z-index: 100;
      color:#fff;
      top:40px;
      margin:0 auto;
      @media (min-width: 768px){
        width: 100%;
      }
      

  `


  const handleClick = () => {
    setIsOpen(!isOpen)
  }
  return (
    <HeaderMain>
     {isMobile ?<HeaderTop>
      <svg onClick={handleClick} width="28" height="28" viewBox="0 0 28 28" fill="none" xmlns="http://www.w3.org/2000/svg">
<g filter="url(#filter0_d_102_328)">
<path d="M7.5 6.6H20.5V5.4H7.5V6.6ZM20.5 8.4H7.5V9.6H20.5V8.4ZM7.5 8.4C7.00294 8.4 6.6 7.99706 6.6 7.5H5.4C5.4 8.6598 6.3402 9.6 7.5 9.6V8.4ZM21.4 7.5C21.4 7.99706 20.9971 8.4 20.5 8.4V9.6C21.6598 9.6 22.6 8.6598 22.6 7.5H21.4ZM20.5 6.6C20.9971 6.6 21.4 7.00294 21.4 7.5H22.6C22.6 6.3402 21.6598 5.4 20.5 5.4V6.6ZM7.5 5.4C6.3402 5.4 5.4 6.3402 5.4 7.5H6.6C6.6 7.00294 7.00294 6.6 7.5 6.6V5.4ZM7.5 13.1H20.5V11.9H7.5V13.1ZM20.5 14.9H7.5V16.1H20.5V14.9ZM7.5 14.9C7.00294 14.9 6.6 14.4971 6.6 14H5.4C5.4 15.1598 6.3402 16.1 7.5 16.1V14.9ZM21.4 14C21.4 14.4971 20.9971 14.9 20.5 14.9V16.1C21.6598 16.1 22.6 15.1598 22.6 14H21.4ZM20.5 13.1C20.9971 13.1 21.4 13.5029 21.4 14H22.6C22.6 12.8402 21.6598 11.9 20.5 11.9V13.1ZM7.5 11.9C6.3402 11.9 5.4 12.8402 5.4 14H6.6C6.6 13.5029 7.00294 13.1 7.5 13.1V11.9ZM7.5 19.6H20.5V18.4H7.5V19.6ZM20.5 21.4H7.5V22.6H20.5V21.4ZM7.5 21.4C7.00294 21.4 6.6 20.9971 6.6 20.5H5.4C5.4 21.6598 6.3402 22.6 7.5 22.6V21.4ZM21.4 20.5C21.4 20.9971 20.9971 21.4 20.5 21.4V22.6C21.6598 22.6 22.6 21.6598 22.6 20.5H21.4ZM20.5 19.6C20.9971 19.6 21.4 20.0029 21.4 20.5H22.6C22.6 19.3402 21.6598 18.4 20.5 18.4V19.6ZM7.5 18.4C6.3402 18.4 5.4 19.3402 5.4 20.5H6.6C6.6 20.0029 7.00294 19.6 7.5 19.6V18.4Z" fill="#4F517C"/>
</g>
<defs>
<filter id="filter0_d_102_328" x="1.39999" y="2.39999" width="25.2" height="25.2" filterUnits="userSpaceOnUse" colorInterpolationFilters="sRGB">
<feFlood floodOpacity="0" result="BackgroundImageFix"/>
<feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
<feOffset dy="1"/>
<feGaussianBlur stdDeviation="2"/>
<feComposite in2="hardAlpha" operator="out"/>
<feColorMatrix type="matrix" values="0 0 0 0 0.00392157 0 0 0 0 0.00784314 0 0 0 0 0.0784314 0 0 0 1 0"/>
<feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_102_328"/>
<feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow_102_328" result="shape"/>
</filter>
</defs>
</svg>
<LogoIcon/>
</HeaderTop> :
<HeaderTop>
<ContentMenu>
        <ul>
          {MenuData.map(item =>
            <li key={item.id}><span>{item.icon}</span>{item.title}</li>
          )}
        </ul>

      </ContentMenu>
<LogoIcon/>
</HeaderTop>
} 
    
      {isOpen ? <ContentMenu>
        <ul>
          {MenuData.map(item =>
            <li key={item.id}><span>{item.icon}</span>{item.title}</li>
          )}
        </ul>

      </ContentMenu> : null}

    </HeaderMain>
  )
}

export default Header