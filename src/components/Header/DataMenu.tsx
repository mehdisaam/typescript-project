import DescriptionOutlinedIcon from '@material-ui/icons/DescriptionOutlined';
import MailOutlineOutlinedIcon from '@material-ui/icons/MailOutlineOutlined';

export const MenuData = [ 
    {
        title:'Home',
        id:1,
},
{
    title:'The Purpose',
    id:2,
},
{
    title:'The Ecosystem',
    id:3,
},
{
    title:'About',
    id:4,
},
{
    title:'Read Whitepaper',
    icon:<DescriptionOutlinedIcon/>,
    id:5,
},
{
    title:'Request Invite',
    icon:<MailOutlineOutlinedIcon/>,
    id:6,
}
]