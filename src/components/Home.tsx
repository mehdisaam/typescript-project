import React from 'react'
import Header from './Header/Header'
import './Home.css';
import IconContent from './IconContent'
import {MainHome,WraperPrim,WraperBlue,WraperBottomBlue,ContentMain} from './Home.style'
const Home = () => {
  return (
      <>
    <MainHome>
        <div className="meain_home">
        <Header/>
        <WraperPrim/>
        <WraperBlue/>
        <WraperBottomBlue/>
        </div>
        
    </MainHome>
    <ContentMain><IconContent/></ContentMain>
    </>
  )
}

export default Home